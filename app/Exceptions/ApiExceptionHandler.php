<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

/**
 * Class ApiExceptionHandler
 * @package DesignerApi\Exceptions
 */
class ApiExceptionHandler
{
    /**
     * @var Handler
     */
    private $appExceptionHandler;

    /**
     * ApiExceptionHandler constructor.
     * @param Handler $exceptionHandler
     */
    public function __construct(Handler $exceptionHandler)
    {
        $this->appExceptionHandler = $exceptionHandler;
    }


    /**
     * @param \Exception $exception
     * @return JsonResponse
     * @throws AuthenticationException
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function handle(\Exception $exception)
    {
        $this->appExceptionHandler->report($exception);

        if ($exception instanceof ModelNotFoundException) {
            return $this->notFoundResponse();
        }

        if ($exception instanceof ValidationException) {
            throw $exception;
        }

        if ($exception instanceof AuthenticationException) {
            throw $exception;
        }

        if ($exception instanceof AuthorizationException) {
            throw $exception;
        }

        return $this->internalErrorResponse();
    }

    /**
     * @return JsonResponse
     */
    private function notFoundResponse()
    {
        return response()->json(['status' => 404, 'error' => 'Not found.'], 404);
    }

    /**
     * @return JsonResponse
     */
    private function internalErrorResponse()
    {
        return response()->json(['status' => 500, 'error' => 'Internal error.'], 500);
    }
}
