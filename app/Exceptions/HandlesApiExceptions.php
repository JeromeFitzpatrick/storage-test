<?php


namespace App\Exceptions;

trait HandlesApiExceptions
{
    /** @var ApiExceptionHandler */
    protected $apiExceptionHandler;

    protected function getApiExceptionHandler()
    {
        if ($this->apiExceptionHandler) {
            return $this->apiExceptionHandler;
        }
        return $this->apiExceptionHandler = app(ApiExceptionHandler::class);
    }

    protected function handleApiException(\Exception $exception)
    {
        return $this->getApiExceptionHandler()->handle(
            $exception
        );
    }
}
