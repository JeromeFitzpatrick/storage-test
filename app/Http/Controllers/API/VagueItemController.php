<?php

namespace App\Http\Controllers\API;

use App\Exceptions\HandlesApiExceptions;
use App\Http\Controllers\Controller;
use App\Http\Transformers\VagueItemTransformer;
use App\Models\Eloquent\EloquentVagueItem;
use App\Models\VagueItem;
use App\Repositories\Criteria\Common\ForOwner;
use App\Repositories\VagueItemRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class VagueItemController extends Controller
{
    use HandlesApiExceptions;

    protected $transformer;
    protected $repo;
    protected $policyReferenceClass = VagueItem::class;

    public function __construct(VagueItemTransformer $transformer, VagueItemRepository $repo)
    {
        $this->transformer = $transformer;
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $this->authorize('viewAny', $this->policyReferenceClass);
            return fractal(
                $this->repo->withCriteria((new ForOwner())->setOwnerId(Auth::id()))->get(),
                $this->transformer
            )->respond();
        } catch (\Exception $exception) {
            return $this->handleApiException($exception);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $this->authorize('create', $this->policyReferenceClass);
            $attributes = $this->validate($request, [
                'email' => 'required|email',
                'message' => 'required|string',
            ]);
            $attributes['owner_id'] = Auth::id();
            $vagueItem = $this->repo->create($attributes);
            return fractal(
                $vagueItem,
                $this->transformer
            )->addMeta(['status'=>'ok','message'=>'Vague Item Created!'])->respond(Response::HTTP_CREATED);
        } catch (\Exception $exception) {
            return $this->handleApiException($exception);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  EloquentVagueItem  $vagueItem
     * @return \Illuminate\Http\Response
     */
    public function show(EloquentVagueItem $vagueItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  EloquentVagueItem  $vagueItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            if (!$vagueItem = $this->repo->find($id)) {
                abort(404);
            }

            $this->authorize('update', $vagueItem);


            $attributes = $this->validate($request, [
                'email' => 'required|email',
                'message' => 'required|string',
            ]);

            if ($this->repo->update($id, $attributes)) {
                $vagueItem = $this->repo->find($id);
            }

            return fractal(
                $vagueItem,
                $this->transformer
            )->respond(Response::HTTP_OK);
        } catch (\Exception $exception) {
            return $this->handleApiException($exception);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if (!$vagueItem = $this->repo->find($id)) {
                return response()->json(['status'=>'Not Found'],Response::HTTP_NOT_FOUND);
            }

            $this->authorize('delete', $vagueItem);

            if ($this->repo->delete($id)) {
                return response()->json(null,Response::HTTP_NO_CONTENT);
            }

            throw new \Exception('Resource was not deleted');

        } catch (\Exception $exception) {
            return $this->handleApiException($exception);
        }
    }
}
