<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class LocalAutoSendToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($user = Auth::guard('web')->user()) {
            $request->headers->add(['Authorization' => 'Bearer ' . $user->getApiToken()]);
        }

        return $next($request);
    }
}
