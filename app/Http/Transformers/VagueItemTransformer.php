<?php
namespace App\Http\Transformers;

use App\Models\VagueItem;
use App\Repositories\UserRepository;
use League\Fractal\TransformerAbstract;


class VagueItemTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['owner'];
    protected $userRepo;
    protected $userTransformer;

    public function __construct(UserRepository $userRepo, UserTransformer $userTransformer)
    {
        $this->userRepo = $userRepo;
        $this->userTransformer = $userTransformer;
    }

    public function transform(VagueItem $item)
    {
        return [
            'id' => $item->getId(),
            'email' => $item->getEmail(),
            'owner_id' => $item->getOwnerId(),
            'message' => $item->getMessage(),
            'created_at' => $item->getCreatedAt()->toDateTimeString(),
            'updated_at' => $item->getUpdatedAt() ? $item->getUpdatedAt()->toDateTimeString() : null,
        ];
    }

    public function includeOwner(VagueItem $vagueItem)
    {
        if ($user = $this->userRepo->find($vagueItem->getOwnerId())) {
            return $this->item($user, $this->userTransformer);
        }
        return null;
    }

}
