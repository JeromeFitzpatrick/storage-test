<?php


namespace App\Models\Cache;


abstract class CacheModel
{
    protected $attributes = [];

    public function __construct(array $attributes = [])
    {
        $this->hydrate($attributes);
    }

    protected function getAttributes(): array
    {
        return $this->attributes;
    }

    public function toArray(): array
    {
        $array = [];
        foreach (array_keys($this->getAttributes()) as $name) {
            $get = 'get' . ucfirst($name);
            $is = 'is' . ucfirst($name);
            $array[$name] = method_exists($this,$get) ? $this->{$get}() : (method_exists($this,$is) ? $this->{$is}() : $this->{$name});
        }
        return $array;
    }

    protected function hydrate(array $attributes)
    {
        foreach ($this->getAttributes() as $name => $options) {
            $value = $attributes[$name] ?? null;
            $this->validate($name, $value);
            $this->{$name} = $value;
        }
        return $this;
    }

    protected function validate($name, $value)
    {
        $attribute = $this->getAttributes()[$name];

        if (!$attribute['nullable'] && is_null($value)) {
            throw new \InvalidArgumentException("{$name} cannot be null");
        }

        if (!in_array(gettype($value),$attribute['type']) && !is_null($value)) {
            $type = gettype($value);
            $expectedType = join(',', $attribute['type']);
            throw new \InvalidArgumentException("{$name} is expected to be of type {$expectedType}, {$type} received instead.");
        }

        if (isset($attribute['class']) && !$value instanceof $attribute['class'] && !is_null($value)) {
            $class = get_class($value);
            $expectedClass = $attribute['class'];
            throw new \InvalidArgumentException("{$name} is expected to be an instance of {$expectedClass}, {$class} received instead.");
        }
    }
}
