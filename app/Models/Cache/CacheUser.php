<?php

namespace App\Models\Cache;

use App\Models\Concerns\HasUserAttributes;
use App\Models\User;
use Carbon\Carbon;
use \Illuminate\Auth\Authenticatable as AuthenticatableTrait;
use Illuminate\Contracts\Support\Arrayable;

/**
 * Class Eloquent
 * @package App\Models
 *
 */
class CacheUser extends CacheModel implements User, Arrayable
{
    use HasUserAttributes, AuthenticatableTrait;

    public $id;
    public $email;
    public $name;
    public $password;
    public $api_token;
    public $created_at;
    public $updated_at;

    protected $attributes = [

        'id' => [
            'type' => ['string'],
            'nullable'=>false
        ],
        'name' => [
            'type' => ['string'],
            'nullable'=>false
        ],
        'email' => [
            'type' => ['string'],
            'nullable'=>false
        ],
        'password' => [
            'type' => ['string'],
            'nullable'=>false
        ],
        'remember_token' => [
            'type' => ['string'],
            'nullable'=>true
        ],
        'api_token' => [
            'type' => ['string'],
            'nullable'=>true
        ],
        'created_at' => [
            'type'=>['object'],
            'class' => Carbon::class,
            'nullable'=>false
        ],
        'updated_at' => [
            'type'=>['object'],
            'class' => Carbon::class,
            'nullable'=>true
        ],
    ];

    protected function getKeyName()
    {
        return 'id';
    }
}
