<?php

namespace App\Models\Cache;

use App\Models\Concerns\HasVagueItemAttributes;
use App\Models\VagueItem;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;

/**
 * Class Eloquent
 * @package App\Models
 *
 */
class CacheVagueItem extends CacheModel implements VagueItem, Arrayable
{
    use HasVagueItemAttributes;

    public $email;
    public $message;
    public $id;
    public $owner_id;
    public $created_at;
    public $updated_at;

    protected $attributes = [
        'email' => [
            'type' => ['string'],
            'nullable'=>false
        ],
        'message' => [
            'type' => ['string'],
            'nullable'=>false
        ],
        'id' => [
            'type' => ['string'],
            'nullable'=>false
        ],
        'owner_id' => [
            'type' => ['string','integer'],
            'nullable'=>false
        ],
        'created_at' => [
            'type'=>['object'],
            'class' => Carbon::class,
            'nullable'=>false
        ],
        'updated_at' => [
            'type'=>['object'],
            'class' => Carbon::class,
            'nullable'=>true
        ],
    ];
}
