<?php

namespace App\Models\Eloquent;

use App\Models\Concerns\HasVagueItemAttributes;
use App\Models\Eloquent\Concerns\UsesUuid;
use App\Models\VagueItem;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Eloquent
 * @package App\Models
 *
 * @property string $id
 * @property string $email
 * @property string $message
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class EloquentVagueItem extends Model implements VagueItem
{
    use UsesUuid, HasVagueItemAttributes;

    /**
     * @var array
     */
    protected $guarded = ['id'];
    protected $table = 'vague_items';
    protected $keyType = 'string';
    public $incrementing = false;

}
