<?php
namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;

interface User extends Authenticatable
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return mixed
     */
    public function getName();

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @return string
     */
    public function getPassword(): ?string;

    /**
     * @return string
     */
    public function getApiToken(): ?string;

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon;

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon;

}
