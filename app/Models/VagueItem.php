<?php


namespace App\Models;


use Carbon\Carbon;

interface VagueItem
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return mixed
     */
    public function getOwnerId();

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @return string
     */
    public function getMessage(): string;

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon;

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon;

}
