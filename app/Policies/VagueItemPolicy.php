<?php

namespace App\Policies;

use App\Models\User;
use App\Models\VagueItem;
use Illuminate\Auth\Access\HandlesAuthorization;

class VagueItemPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any vague items.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the vague item.
     *
     * @param  \App\Models\User  $user
     * @param  VagueItem  $vagueItem
     * @return mixed
     */
    public function view(User $user, VagueItem $vagueItem)
    {
        return true;
    }

    /**
     * Determine whether the user can create vague items.
     *
     * @param  \App\Models\Eloquent\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the vague item.
     *
     * @param  \App\Models\User  $user
     * @param  VagueItem  $vagueItem
     * @return mixed
     */
    public function update(User $user, VagueItem $vagueItem)
    {
        return $user->getId() == $vagueItem->getOwnerId();
    }

    /**
     * Determine whether the user can delete the vague item.
     *
     * @param  \App\Models\User  $user
     * @param  VagueItem  $vagueItem
     * @return mixed
     */
    public function delete(User $user, VagueItem $vagueItem)
    {
        return $user->getId() == $vagueItem->getOwnerId() || !$vagueItem->getOwnerId();
    }

    /**
     * Determine whether the user can restore the vague item.
     *
     * @param  \App\Models\User  $user
     * @param  VagueItem  $vagueItem
     * @return mixed
     */
    public function restore(User $user, VagueItem $vagueItem)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the vague item.
     *
     * @param  \App\Models\User  $user
     * @param  VagueItem  $vagueItem
     * @return mixed
     */
    public function forceDelete(User $user, VagueItem $vagueItem)
    {
        //
    }
}
