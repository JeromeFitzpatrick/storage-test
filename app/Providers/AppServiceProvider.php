<?php

namespace App\Providers;

use App\Models\Eloquent\EloquentVagueItem;
use App\Models\Eloquent\User;
use App\Repositories\Cache\CacheUserRepository;
use App\Repositories\Cache\CacheVagueItemRepository;
use App\Repositories\Cache\Criteria\CacheCriteriaFactory;
use App\Repositories\Criteria\CriteriaFactory;
use App\Repositories\Eloquent\Criteria\EloquentCriteriaFactory;
use App\Repositories\Eloquent\EloquentUserRepository;
use App\Repositories\Eloquent\EloquentVagueItemRepository;
use App\Repositories\UserRepository;
use App\Repositories\VagueItemRepository;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->when([CacheUserRepository::class, CacheVagueItemRepository::class])
            ->needs(CriteriaFactory::class)
            ->give(function ($app) {
                return $app->make(CacheCriteriaFactory::class);
            });


        $this->app->when([EloquentUserRepository::class, EloquentVagueItemRepository::class])
            ->needs(CriteriaFactory::class)
            ->give(function ($app) {
                return $app->make(EloquentCriteriaFactory::class);
            });

        if (config('persistence.users.store','cache') == 'cache') {
            $this->app->bind(UserRepository::class,CacheUserRepository::class);
        }

        if (config('persistence.users.store','cache') == 'eloquent') {
            $this->app->bind(UserRepository::class, function ($app) {
                return new EloquentUserRepository($app->make(EloquentCriteriaFactory::class), new User());
            });
        }

        if (config('persistence.vague_items.store','cache') == 'cache') {
            $this->app->bind(VagueItemRepository::class, CacheVagueItemRepository::class);
        }

        if (config('persistence.vague_items.store','cache') == 'eloquent') {
            $this->app->bind(VagueItemRepository::class, function ($app) {
                return new EloquentVagueItemRepository($app->make(EloquentCriteriaFactory::class),new EloquentVagueItem());
            });
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
