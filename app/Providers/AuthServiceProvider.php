<?php

namespace App\Providers;

use App\Models\VagueItem;
use App\Policies\VagueItemPolicy;
use App\Repositories\Cache\CacheUserRepository;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
         VagueItem::class => VagueItemPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('cache', function ($app, array $config) {
            return $app->make(CacheUserRepository::class);
        });
    }
}
