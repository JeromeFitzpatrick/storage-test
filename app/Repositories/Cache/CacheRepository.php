<?php
namespace App\Repositories\Cache;

use App\Repositories\Cache\Criteria\CacheCriteriaFactory;
use App\Repositories\Criteria\CriteriaFactory;
use App\Repositories\Repository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

/**
 * Class EloquentRepositoryAbstract
 * @package App\Repositories\Eloquent
 */
abstract class CacheRepository implements Repository
{
    protected $updateableFields = [];
    protected $wheres = [];
    protected $criteria = [];
    protected $keyName = 'id';
    protected $criteriaFactory;

    public function __construct(CriteriaFactory $criteriaFactory)
    {
        $this->criteriaFactory = $criteriaFactory;
    }

    /**
     * @return string
     */
    abstract protected function getCacheIdentifier(): string;

    /**
     * @return string
     */
    abstract protected function getCacheModelClass(): string;

    /**
     * @return array
     */
    protected function getUpdateableFields()
    {
        return $this->updateableFields;
    }

    /**
     * @param $id
     * @return \stdClass|null|Model
     */
    public function find($id)
    {
        $dataCollection = $this->get();
        return $dataCollection->get($id);
    }

    /**
     * @return Collection
     */
    public function get(): Collection
    {
        $result = Cache::get($this->getCacheIdentifier()) ?? collect([]);
        foreach ($this->criteria as $criterion) {
            $result = $criterion->apply($result);
        }
        return $result;
    }

    public function first()
    {
        return $this->get()->first();
    }

    /**
     * @param int $perPage
     * @return mixed
     * @throws \Exception
     */
    public function paginate(int $perPage = 15)
    {
        throw new \Exception('Paginate is not implemented for cache repositories');
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id): bool
    {
        if ($dataCollection = $this->get()) {
            if ($dataCollection->forget($id)) {
                return $this->save($dataCollection);
            }
        }
        return false;
    }

    /**
     * @param $id
     * @param array $attributes
     * @return bool
     */
    public function update($id, array $attributes): bool
    {
        $modelClass = $this->getCacheModelClass();
        if (!$dataCollection = $this->get()) {
            return false;
        }

        if (!$model = $dataCollection->get($id)) {
            return false;
        }

        $modelArray = $model->toArray();

        foreach ($this->getUpdateableFields() as $name) {
            $modelArray[$name] = $attributes[$name] ?? $modelArray[$name];
        }

        if (array_diff($modelArray, $model->toArray())) {
            $modelArray['updated_at'] = Carbon::now();
            $dataCollection->put($id,new $modelClass($modelArray));
            return $this->save($dataCollection);
        }
        return false;
    }

    /**
     * @param array $attributes
     * @return mixed
     * @throws \Exception
     */
    public function create(array $attributes)
    {
        if ($this->get()->firstWhere($this->getKeyName(),$attributes[$this->getKeyName()])) {
            throw new \Exception('Duplicate key exception');
        }

        $modelClass = $this->getCacheModelClass();
        $model = new $modelClass($attributes);
        $dataCollection = Cache::get($this->getCacheIdentifier()) ?? collect([]);
        $dataCollection->put($attributes['id'], $model);
        $this->save($dataCollection);
        return $model;
    }

    protected function save(Collection $dataCollection): bool
    {
        return Cache::forever($this->getCacheIdentifier(),$dataCollection);
    }

    public function withCriteria(...$criteria): Repository
    {
        $criteria = Arr::flatten($criteria);
        foreach ($criteria as $criterion) {
            $this->criteria[] = $this->criteriaFactory->make($criterion);
        }
        return $this;
    }

    protected function getKeyName()
    {
        return $this->keyName;
    }
}
