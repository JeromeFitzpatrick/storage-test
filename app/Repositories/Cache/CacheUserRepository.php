<?php

namespace App\Repositories\Cache;

use App\Models\Cache\CacheUser;
use App\Models\User;
use App\Models\VagueItem;
use App\Repositories\Criteria\CriteriaFactory;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Class EloquentVagueItemRepository
 * @package App\Repositories\Eloquent
 */
class CacheUserRepository extends CacheRepository implements UserRepository, UserProvider
{

    /**
     * @var string
     */
    protected $cacheIdentifier;
    /**
     * @var string
     */
    protected $cacheModelClass = CacheUser::class;

    /**
     * @var array
     */
    protected $updateableFields = [
        'email','password','name'
    ];

    /**
     * The hasher implementation.
     *
     * @var \Illuminate\Contracts\Hashing\Hasher
     */
    protected $hasher;


    /**
     * CacheUserRepository constructor.
     * @param HasherContract $hasher
     * @param CriteriaFactory $criteriaFactory
     */
    public function __construct(
        HasherContract $hasher,
        CriteriaFactory $criteriaFactory)
    {
        $this->cacheIdentifier= config('persistence.users.name','vague_users');
        $this->hasher = $hasher;
        parent::__construct($criteriaFactory);
    }

    /**
     * @return string
     */
    protected function getCacheIdentifier(): string
    {
        return $this->cacheIdentifier;
    }

    /**
     * @return string
     */
    protected function getCacheModelClass(): string
    {
        return $this->cacheModelClass;
    }

    /**
     * @return Collection|VagueItem[]
     */
    public function get(): Collection
    {
        return parent::get();
    }

    /**
     * @param $id
     * @return User|null
     */
    public function find($id): ?User
    {
        return parent::find($id);
    }

    /**
     * @param array $attributes
     * @return User
     */
    public function create(array $attributes): User
    {
        $attributes['id'] = Str::uuid()->toString();
        $attributes['created_at'] = Carbon::now();
        $attributes['updated_at'] = Carbon::now();
        return parent::create($attributes);
    }


    /** User Provider Methods */




    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed  $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        return $this->find($identifier);
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed  $identifier
     * @param  string  $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        if (!$user = $this->get()->where($this->getKeyName(), '=',$identifier)->first()) {
            return null;
        }

        $rememberToken = $user->getRememberToken();

        return $rememberToken && hash_equals($rememberToken, $token)
            ? $user : null;
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable|User $user
     * @param  string  $token
     * @return void
     */
    public function updateRememberToken(UserContract $user, $token)
    {
        $this->update($user->getId(),['remember_token'=>$token]);
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        if (empty($credentials) ||
            (count($credentials) === 1 &&
                array_key_exists('password', $credentials))) {
            return;
        }

        $dataCollection = $this->get();

        foreach ($credentials as $key => $value) {
            if (Str::contains($key, 'password')) {
                continue;
            }
            $dataCollection = $dataCollection->where($key, '=',$value);
        }

        return $dataCollection->first();
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     */
    public function validateCredentials(UserContract $user, array $credentials)
    {
        $plain = $credentials['password'];

        return $this->hasher->check($plain, $user->getAuthPassword());
    }

    /**
     * Gets the hasher implementation.
     *
     * @return \Illuminate\Contracts\Hashing\Hasher
     */
    public function getHasher()
    {
        return $this->hasher;
    }

    /**
     * Sets the hasher implementation.
     *
     * @param  \Illuminate\Contracts\Hashing\Hasher  $hasher
     * @return $this
     */
    public function setHasher(HasherContract $hasher)
    {
        $this->hasher = $hasher;

        return $this;
    }

}
