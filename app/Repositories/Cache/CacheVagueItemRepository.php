<?php

namespace App\Repositories\Cache;

use App\Models\Cache\CacheVagueItem;
use App\Models\VagueItem;
use App\Repositories\Criteria\CriteriaFactory;
use App\Repositories\UserRepository;
use App\Repositories\VagueItemRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Class EloquentVagueItemRepository
 * @package App\Repositories\Eloquent
 */
class CacheVagueItemRepository extends CacheRepository implements VagueItemRepository
{
    /**
     * @var
     */
    protected $cacheIdentifier;
    protected $updateableFields = ['email','message'];
    protected $cacheModelClass = CacheVagueItem::class;

    /**
     * @var null
     */
    protected $forUser = null;


    /**
     * CacheVagueItemRepository constructor.
     * @param CriteriaFactory $criteriaFactory
     */
    public function __construct(CriteriaFactory $criteriaFactory)
    {
        parent::__construct($criteriaFactory);
        $this->cacheIdentifier= config('persistence.vague_items.name','vague_items');
    }

    protected function getCacheModelClass(): string
    {
        return $this->cacheModelClass;
    }

    /**
     * @param $userId
     * @return VagueItemRepository
     */
    public function forUser($userId): VagueItemRepository
    {
        $this->forUser = $userId;
        return $this;
    }

    /**
     * @return string
     */
    protected function getCacheIdentifier(): string
    {
        return $this->cacheIdentifier;
    }

    /**
     * @return Collection|VagueItem[]
     */
    public function get(): Collection
    {
        return parent::get();
    }

    /**
     * @param $id
     * @return VagueItem|null
     */
    public function find($id): ?VagueItem
    {
        return parent::find($id);
    }

    public function create(array $attributes): VagueItem
    {
        $attributes['id'] = Str::uuid()->toString();
        $attributes['created_at'] = Carbon::now();
        $attributes['updated_at'] = Carbon::now();
        return parent::create($attributes);
    }
}
