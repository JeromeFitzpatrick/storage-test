<?php
namespace App\Repositories\Cache\Criteria;


use App\Repositories\Criteria\Common\ForOwner;
use App\Repositories\Criteria\CriteriaFactory;
use App\Repositories\Criteria\CriteriaFactoryAbstract;

class CacheCriteriaFactory extends CriteriaFactoryAbstract implements CriteriaFactory
{
    protected $map = [
        ForOwner::class => CacheForOwner::class
    ];
}
