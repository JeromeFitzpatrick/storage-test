<?php


namespace App\Repositories\Cache\Criteria;


use Illuminate\Support\Collection;

interface CacheCriterion
{
    public function apply(Collection $collection): Collection;
}
