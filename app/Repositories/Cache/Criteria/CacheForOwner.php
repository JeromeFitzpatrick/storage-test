<?php
namespace App\Repositories\Cache\Criteria;

use App\Repositories\Criteria\Common\ForOwner;
use App\Repositories\Criteria\CriterionAbstract;
use Illuminate\Support\Collection;

class CacheForOwner extends CriterionAbstract implements CacheCriterion
{

    public function getBase(): ForOwner
    {
        return $this->base;
    }

    public function apply(Collection $collection): Collection
    {
        return $collection->where('owner_id', $this->getBase()->getOwnerId());
    }
}
