<?php
namespace App\Repositories\Criteria\Common;


use App\Repositories\Criteria\CriterionBase;

class ForOwner implements CriterionBase
{
    protected $ownerId;

    /**
     * @param mixed $ownerId
     * @return ForOwner
     */
    public function setOwnerId($ownerId): ForOwner
    {
        $this->ownerId = $ownerId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }
}
