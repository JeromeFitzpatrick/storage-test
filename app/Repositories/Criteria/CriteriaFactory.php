<?php


namespace App\Repositories\Criteria;


interface CriteriaFactory
{
    public function make(CriterionBase $criterion): Criterion;
}
