<?php


namespace App\Repositories\Criteria;


abstract class CriteriaFactoryAbstract implements CriteriaFactory
{
    protected $map;

    public function make(CriterionBase $criterionBase): Criterion
    {
        if ($concrete = $this->create($criterionBase)) {
            return $concrete;
        }

        throw new \Exception('Criteria Not Implemented' . get_class($criterionBase));
    }

    protected function create(CriterionBase $criterionBase)
    {
        $criterionBaseClass = get_class($criterionBase);
        if (array_key_exists($criterionBaseClass, $this->map)) {
            $concreteClass = $this->map[$criterionBaseClass];
            /** @var Criterion $concreteObject */
            $concreteObject = new $concreteClass();
            return $concreteObject->setBase($criterionBase);
        }
        return null;
    }
}
