<?php
namespace App\Repositories\Criteria;

interface Criterion
{
    public function setBase($criterionBase);
    public function getBase();
}
