<?php
namespace App\Repositories\Criteria;

abstract class CriterionAbstract implements Criterion
{
    protected $base;

    public function setBase($criterionBase)
    {
        $this->base = $criterionBase;
        return $this;
    }

    public function getBase()
    {
        return $this->base;
    }
}
