<?php
namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\Criteria\Common\ForOwner;
use App\Repositories\Criteria\CriteriaFactory;
use App\Repositories\Criteria\CriteriaFactoryAbstract;

class EloquentCriteriaFactory extends CriteriaFactoryAbstract implements CriteriaFactory
{
    protected $map = [
        ForOwner::class => EloquentForOwner::class
    ];
}
