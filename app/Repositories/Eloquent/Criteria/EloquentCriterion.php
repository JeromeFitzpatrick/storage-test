<?php
namespace App\Repositories\Eloquent\Criteria;

use Illuminate\Database\Eloquent\Builder;

interface EloquentCriterion
{
    public function apply(Builder $collection): Builder;
}


