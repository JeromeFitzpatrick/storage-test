<?php
namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\Criteria\Common\ForOwner;
use App\Repositories\Criteria\CriterionAbstract;
use Illuminate\Database\Eloquent\Builder;

class EloquentForOwner extends CriterionAbstract implements EloquentCriterion
{

    public function getBase(): ForOwner
    {
        return $this->base;
    }

    public function apply(Builder $builder): Builder
    {
        return $builder->where('owner_id', $this->getBase()->getOwnerId());
    }
}
