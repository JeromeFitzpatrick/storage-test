<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Criteria\CriteriaFactory;
use App\Repositories\Repository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Pagination\PaginatorInterface;

/**
 * Class EloquentRepositoryAbstract
 * @package App\Repositories\Eloquent
 */
abstract class EloquentRepository implements Repository
{

    protected $criteria = [];
    protected $criteriaFactory;

    /**
     * @return Model
     */
    abstract protected function getModel(): Model;


    public function __construct(CriteriaFactory $criteriaFactory)
    {
        $this->criteriaFactory = $criteriaFactory;
    }

    /**
     * @return Builder
     */
    protected function getQuery(): Builder
    {
        return $this->getModel()->query();
    }

    public function first()
    {
        return $this->getQuery()->first();
    }

    /**
     * @param $id
     * @return \stdClass|null|Model
     */
    public function find($id)
    {
        return $this->getQuery()->find($id);
    }

    /**
     * @return Collection
     */
    public function get(): Collection
    {
        return $this->getQuery()->get();
    }

    /**
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function paginate(int $perPage = 15): PaginatorInterface
    {
        $page = $this->getQuery()->paginate($perPage);
        return new IlluminatePaginatorAdapter($page);
    }

    /**
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function delete($id): bool
    {
        if ($resource = $this->find($id)) {
            return $resource->delete();
        }
        return false;
    }

    /**
     * @param $id
     * @param array $attributes
     * @return bool
     */
    public function update($id, array $attributes): bool
    {
        return $this->getQuery()->where($this->getModel()->getKeyName(), $id)->update(
            $attributes
        );
    }

    /**
     * @param array $attributes
     * @return object|Model
     */
    public function create(array $attributes)
    {
        return $this->getQuery()->create($attributes);
    }

    public function withCriteria(...$criteria): Repository
    {
        $criteria = Arr::flatten($criteria);
        foreach ($criteria as $criterion) {
            $this->criteria[] = $this->criteriaFactory->make($criterion);
        }
        return $this;
    }


}
