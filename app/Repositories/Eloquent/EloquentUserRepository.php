<?php
namespace App\Repositories\Eloquent;

use App\Models\User;
use App\Repositories\Criteria\CriteriaFactory;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class EloquentVagueItemRepository
 * @package App\Repositories\Eloquent
 */
class EloquentUserRepository extends EloquentRepository implements UserRepository
{
    /**
     * @var
     */
    protected $model;

    /**
     * EloquentVagueItemRepository constructor.
     * @param Model $model
     * @param CriteriaFactory $criteriaFactory
     */
    public function __construct(CriteriaFactory $criteriaFactory, Model $model)
    {
        parent::__construct($criteriaFactory);
        $this->model = $model;
        $this->criteriaFactory = $criteriaFactory;
    }

    /**
     * @return Model
     */
    protected function getModel(): Model
    {
        return $this->model;
    }

    /**
     * @return Collection|User[]
     */
    public function get(): Collection
    {
        return parent::get();
    }

    /**
     * @param $id
     * @return User|null
     */
    public function find($id): ?User
    {
        return parent::find($id);
    }

    /**
     * @param array $attributes
     * @return User
     */
    public function create(array $attributes): User
    {
        return parent::create($attributes);
    }
}
