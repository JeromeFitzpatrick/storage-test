<?php


namespace App\Repositories\Eloquent;


use App\Models\VagueItem;
use App\Repositories\Criteria\CriteriaFactory;
use App\Repositories\VagueItemRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class EloquentVagueItemRepository
 * @package App\Repositories\Eloquent
 */
class EloquentVagueItemRepository extends EloquentRepository implements VagueItemRepository
{
    /**
     * @var
     */
    protected $model;

    /**
     * EloquentVagueItemRepository constructor.
     * @param Model $model
     * @param CriteriaFactory $criteriaFactory
     */
    public function __construct(CriteriaFactory $criteriaFactory, Model $model)
    {
        parent::__construct($criteriaFactory);
        $this->model = $model;
        $this->criteriaFactory = $criteriaFactory;
    }

    /**
     * @return Model
     */
    protected function getModel(): Model
    {
        return $this->model;
    }

    /**
     * @return Collection|VagueItem[]
     */
    public function get(): Collection
    {
        return parent::get();
    }

    /**
     * @param $id
     * @return VagueItem|null
     */
    public function find($id): ?VagueItem
    {
        return parent::find($id);
    }

    /**
     * @param array $attributes
     * @return VagueItem
     */
    public function create(array $attributes): VagueItem
    {
        return parent::create($attributes);
    }

    public function forUser($userId): VagueItemRepository
    {
        return $this->getQuery()->where('owner_id',$userId)->get();
    }
}
