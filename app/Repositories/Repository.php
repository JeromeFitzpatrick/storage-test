<?php
namespace App\Repositories;

use Illuminate\Support\Collection;

interface Repository
{
    public function paginate(int $perPage = 15);
    public function get(): Collection;
    public function find($id);
    public function first();
    public function create(array $attributes);
    public function update($id, array $attributes): bool;
    public function delete($id): bool;

    /**
     * @param mixed ...$criteria
     * @return $this
     */
    public function withCriteria(...$criteria): Repository ;

}
