<?php
namespace App\Repositories;

use App\Models\User;

interface UserRepository extends Repository
{
    public function find($id): ?User;
    public function create(array $attributes): User;
}
