<?php
namespace App\Repositories;

use App\Models\VagueItem;

interface VagueItemRepository extends Repository
{
    public function find($id): ?VagueItem;
    public function create(array $attributes): VagueItem;
    public function forUser($userId): VagueItemRepository;
}
