<?php
/*
|--------------------------------------------------------------------------
| Persistence
|--------------------------------------------------------------------------
|
| Specify the type of persistence to use for each model. Cache and Eloquent
| currently supported. Note that name is only applicable to the Cache store.
| `eloquent_model` is only applicable when using the eloquent store.
| Modify the table attribute in the eloquent model to change the name of
| the eloquent store.
|
| Store types Supported: "cache", "eloquent",
|
*/

return [
    'users' => [
        'store' => env('USERS_STORE','cache'),
        'name' => env('CACHE_USERS_NAME','vague_users'),
        'eloquent_model' => \App\Models\Eloquent\User::class,
    ],
    'vague_items' => [
        'store' => env('VAGUE_ITEMS_STORE','cache'),
        'name' => env('CACHE_VAGUE_ITEMS_NAME','vague_items'),
    ],
];
